﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZzukBot.Constants;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers.PPather;

namespace GrindBot.Engine.Grind
{
    internal class _StuckHelper
    {
        
        private float diffToPoint;


        private Location oldPosition = new Location(0, 0, 0);
        private int StuckAtPointSince;

        internal _StuckHelper()
        {
            WoWEventHandler.Instance.OnCtm += CtmAreWeStuck;
        }

        ~_StuckHelper()
        {
            WoWEventHandler.Instance.OnCtm -= CtmAreWeStuck;
        }

        internal void CtmAreWeStuck(object n, WoWEventHandler.OnCtmArgs e)
        {

            var parType = (Constants.Enums.CtmType)e.CtmType;
            var parPosition = e.Position;

            switch (parType)
            {
                case Constants.Enums.CtmType.Move:
                    if (oldPosition.GetDistanceTo(parPosition) > 1.0f)
                    {
                        oldPosition = parPosition;
                        StuckAtPointSince = Environment.TickCount;
                        diffToPoint = ObjectManager.Instance.Player.Position.GetDistanceTo(parPosition);
                        //Hacks.Instance.Collision1 = false;
                        //Hacks.Instance.Collision2 = false;
                    }
                    else
                    {
                        var newDiffToPoint = ObjectManager.Instance.Player.Position.GetDistanceTo(parPosition);
                        if (Math.Abs(newDiffToPoint - diffToPoint) > 1.5f || newDiffToPoint < 1.3f)
                        {
                            diffToPoint = newDiffToPoint;
                            StuckAtPointSince = Environment.TickCount;
                        }
                        else if (Environment.TickCount - StuckAtPointSince > 1000 &&
                                 Environment.TickCount - StuckAtPointSince < 2000)
                        {
                            //Mark the stuck
                            Navigation.Instance.MarkStuckAt(ObjectManager.Instance.Player.Position, ObjectManager.Instance.Player.Facing);
                            //Attempt to jump out!
                            Lua.Instance.Execute("Jump()");

                        }else if (Environment.TickCount - StuckAtPointSince > 2100 &&
                                 Environment.TickCount - StuckAtPointSince < 3200)
                        {
                            KeySender.Instance.SendDown(Keys.Left);
                        }
                        else
                        {
                            KeySender.Instance.SendUp(Keys.Left);
                            //Hacks.Instance.Collision1 = true;
                            //Hacks.Instance.Collision2 = true;
                            //Lua.Instance.Execute("Jump()");
                        }
                    }
                    break;

                case Constants.Enums.CtmType.None:
                case Constants.Enums.CtmType.Idle:
                    StuckAtPointSince = Environment.TickCount;
                    oldPosition = new Location(0, 0, 0);
                    //Hacks.Instance.Collision1 = false;
                    //Hacks.Instance.Collision2 = false;
                    KeySender.Instance.SendUp(Keys.Left);
                    break;
            }
        }

        internal void Reset()
        {
            StuckAtPointSince = 0;
            oldPosition = new Location(0, 0, 0);
            //Hacks.Instance.Collision1 = false;
            //Hacks.Instance.Collision2 = false;
        }
    }
}
