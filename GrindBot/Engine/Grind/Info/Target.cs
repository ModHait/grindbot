﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrindBot.Settings;
using ZzukBot.Constants;
using ZzukBot.ExtensionFramework;
using ZzukBot.Game.Statics;
using ZzukBot.Objects;

namespace GrindBot.Engine.Grind.Info
{
    internal class _Target
    {
        internal volatile bool FixFacing;
        internal int HostileMobRange = 15;

        internal volatile bool InSightWithTarget;
        internal volatile int ResetToNormalAt = 0;


        internal bool SearchDirect = true;

        internal _Target()
        {
            InSightWithTarget = true;
            FixFacing = false;
        }

        // Get the next mob we should attack
        internal WoWUnit NextTarget
        {
            get
            {

                Constants.Enums.SearchType SearchType = Constants.Enums.SearchType.All;
                //Confusing logic with both factions and ids
                if (Grinder.Access.Profile.Factions == null && Grinder.Access.Profile.Ids == null)
                {
                    SearchType = Constants.Enums.SearchType.All;
                }
                else if (Grinder.Access.Profile.Factions == null && Grinder.Access.Profile.Ids != null)
                {
                    SearchType = Constants.Enums.SearchType.Id;
                }
                else if (Grinder.Access.Profile.Factions != null && Grinder.Access.Profile.Ids == null)
                {
                    SearchType = Constants.Enums.SearchType.Faction;
                }
                else
                {
                    SearchType = Constants.Enums.SearchType.Both;
                }

                return getTarg(SearchType);





                /*
                mobs = mobs
                    .Where(
                        i =>
                            i.IsMob && i.Health != 0 && !Grinder.Access.Info.Combat.BlacklistContains(i.Guid) &&
                            ((SearchType == Constants.Enums.SearchType.All || 
                            SearchType == Constants.Enums.SearchType.Id &&
                            Grinder.Access.Profile.Ids.Contains(i.Id) ||
                            SearchType == Constants.Enums.SearchType.Faction &&
                            Grinder.Access.Profile.Factions.Contains(i.FactionId) ||
                            SearchType == Constants.Enums.SearchType.Both &&
                            (Grinder.Access.Profile.Factions.Contains(i.FactionId) || Grinder.Access.Profile.Ids.Contains(i.Id)))) &&
                            i.Reaction != Enums.UnitReaction.Friendly && !i.IsCritter && i.IsUntouched &&
                            i.HealthPercent == 100 &&
                            i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position) <= GrindSettings.Values.MobSearchRange &&
                            Math.Abs(ObjectManager.Instance.Player.Position.Z - i.Position.Z) <= 4 && i.SummonedBy == 0 &&
                            i.TargetGuid == 0)
                    .OrderBy(i => i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position)).ToList();
                return mobs.FirstOrDefault();
                */
            }
        }

        // should we attack our current target?
        internal bool ShouldAttackTarget
        {
            get
            {
                var target = ObjectManager.Instance.Target;
                if (target == null) return false;
                var ret =
                    target.IsMob && !Grinder.Access.Info.Combat.BlacklistContains(target.Guid) &&
                    target.Reaction != Enums.UnitReaction.Friendly /*&&
                    (target.IsUntouched || target.IsMarked)*/&& !target.TappedByOther && target.HealthPercent == 100
                    && !target.IsCritter;

                if (!ret)
                {
                    ObjectManager.Instance.Player.SetTarget(0);

                    if (ObjectManager.Instance.Player.Casting != 0 || ObjectManager.Instance.Player.Channeling != 0)
                    {
                        //ObjectManager.Instance.Player.StartMovement(Enums.ControlBits.Front);
                        //ObjectManager.Instance.Player.StopMovement(Enums.ControlBits.Front);
                    }
                }
                return ret;
            }
        }

        // In range ton attack target
        internal float CombatDistance
        {
            get
            {
                var tmp = CustomClasses.Instance.Current.CombatDistance;
                if (!InSightWithTarget)
                {
                    if (!Grinder.Access.Info.Combat.IsMovingBack)
                        tmp = 3;
                    if (Environment.TickCount >= ResetToNormalAt)
                        InSightWithTarget = true;
                }
                return tmp;
            }
        }

        private WoWUnit getTarg(Constants.Enums.SearchType parSearchType)
        {
            var mobs = ObjectManager.Instance.Npcs;

            int minLevel = 1;
            int maxLevel = 100;

            if (Grinder.Access.Profile.MinLevel > 1)
            {
                minLevel = Grinder.Access.Profile.MinLevel;
            }
            if (Grinder.Access.Profile.MaxLevel < 100)
            {
                maxLevel = Grinder.Access.Profile.MaxLevel;
            }

            if (parSearchType == Constants.Enums.SearchType.All)
            {
                mobs = mobs
                   .Where(
                       i =>
                           i.IsMob && i.Health != 0 && !Grinder.Access.Info.Combat.BlacklistContains(i.Guid) &&
                           i.Reaction != Enums.UnitReaction.Friendly && !i.IsCritter && i.IsUntouched &&
                           i.HealthPercent == 100 &&
                           i.Level >= minLevel && i.Level <= maxLevel &&
                            i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position) <= GrindSettings.Values.MobSearchRange &&
                            Math.Abs(ObjectManager.Instance.Player.Position.Z - i.Position.Z) <= 4 && i.SummonedBy == 0 &&
                            i.TargetGuid == 0)
                    .OrderBy(i => i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position)).ToList();
            }
            else if (parSearchType == Constants.Enums.SearchType.Id)
            {
                mobs = mobs
                    .Where(
                        i =>
                            i.IsMob && i.Health != 0 && !Grinder.Access.Info.Combat.BlacklistContains(i.Guid) &&
                            Grinder.Access.Profile.Ids.Contains(i.Id) &&
                            i.Reaction != Enums.UnitReaction.Friendly && !i.IsCritter && i.IsUntouched &&
                            i.HealthPercent == 100 &&
                            i.Level >= minLevel && i.Level <= maxLevel &&
                            i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position) <= GrindSettings.Values.MobSearchRange &&
                            Math.Abs(ObjectManager.Instance.Player.Position.Z - i.Position.Z) <= 4 && i.SummonedBy == 0 &&
                            i.TargetGuid == 0)
                    .OrderBy(i => i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position)).ToList();
            }
            else if (parSearchType == Constants.Enums.SearchType.Faction)
            {
                mobs = mobs
                    .Where(
                        i =>
                            i.IsMob && i.Health != 0 && !Grinder.Access.Info.Combat.BlacklistContains(i.Guid) &&
                            Grinder.Access.Profile.Factions.Contains(i.FactionId) &&
                            i.Reaction != Enums.UnitReaction.Friendly && !i.IsCritter && i.IsUntouched &&
                            i.HealthPercent == 100 &&
                            i.Level >= minLevel && i.Level <= maxLevel &&
                            i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position) <=
                            GrindSettings.Values.MobSearchRange &&
                            Math.Abs(ObjectManager.Instance.Player.Position.Z - i.Position.Z) <= 4 && i.SummonedBy == 0 &&
                            i.TargetGuid == 0)
                    .OrderBy(i => i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position)).ToList();
            }
            else
            {
                mobs = mobs
                    .Where(
                        i =>
                            i.IsMob && i.Health != 0 && !Grinder.Access.Info.Combat.BlacklistContains(i.Guid) &&
                            (Grinder.Access.Profile.Factions.Contains(i.FactionId) || Grinder.Access.Profile.Ids.Contains(i.Id)) &&
                            i.Reaction != Enums.UnitReaction.Friendly && !i.IsCritter && i.IsUntouched &&
                            i.HealthPercent == 100 &&
                            i.Level >= minLevel && i.Level <= maxLevel &&
                            i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position) <=
                            GrindSettings.Values.MobSearchRange &&
                            Math.Abs(ObjectManager.Instance.Player.Position.Z - i.Position.Z) <= 4 && i.SummonedBy == 0 &&
                            i.TargetGuid == 0)
                    .OrderBy(i => i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position)).ToList();
            }


            return mobs.FirstOrDefault();
        }
    }
}
