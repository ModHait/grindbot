﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZzukBot.Objects;
using ZzukBot.Game.Statics;
using ZzukBot.Constants;
using ZzukBot.Helpers;

namespace GrindBot.Engine.Grind.Info
{
    internal class _Combat
    {
        private int lastCheck;

        internal int LastFightTick = 0;

        internal _Combat()
        {
            BlacklistedUnits = new List<ulong>();
            OldGuid = 0;
            OldHpPercent = 100;
        }

        // Mobs that attck us

        private List<ulong> BlacklistedUnits { get; }
        private ulong OldGuid { get; set; }
        private float OldHpPercent { get; set; }

        internal bool IsMoving => //Grinder.Access.Info.PathBackup.MovingBack || Grinder.Access.Info.PathForceBackup.MovingBack ||
                                  Grinder.Access.Info.Target.FixFacing || !Grinder.Access.Info.Target.InSightWithTarget;

        internal bool IsMovingBack => false;//Grinder.Access.Info.PathBackup.MovingBack || Grinder.Access.Info.PathForceBackup.MovingBack;


        internal bool IsBlacklisted(WoWUnit parUnit)
        {
            if (parUnit == null) return false;
            if (BlacklistedUnits.Contains(parUnit.Guid))
                return true;

            if (OldGuid != parUnit.Guid)
            {
                OldGuid = parUnit.Guid;
                OldHpPercent = parUnit.HealthPercent;
                Wait.Remove("UnitBlacklist");
                lastCheck = Environment.TickCount;
            }
            else
            {
                if (Environment.TickCount - lastCheck >= 5000)
                {
                    Wait.Remove("UnitBlacklist");
                }
                lastCheck = Environment.TickCount;
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (OldHpPercent == parUnit.HealthPercent)
                {
                    if (Wait.For("UnitBlacklist", 25000))
                    {
                        if (!BlacklistedUnits.Contains(parUnit.Guid))
                            BlacklistedUnits.Add(parUnit.Guid);
                    }
                }
                else
                    OldGuid = 0;
            }
            return false;
        }

        internal void AddToBlacklist(ulong parGuid)
        {
            if (!BlacklistedUnits.Contains(parGuid))
                BlacklistedUnits.Add(parGuid);
        }

        internal bool BlacklistContains(ulong parGuid)
        {
            return BlacklistedUnits.Contains(parGuid);
        }
    }
}