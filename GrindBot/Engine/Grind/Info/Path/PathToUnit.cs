﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrindBot.Settings;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers.PPather;
using ZzukBot.Objects;

namespace GrindBot.Engine.Grind.Info.Path
{
    internal class _PathToUnit
    {
        private ulong unitLastGuid;
        private int lastUpdate = Environment.TickCount;

        internal _PathToUnit()
        {
            unitLastGuid = 0;
        }

        private Location unitLastPos { get; set; }
        private int unitLastWaypointIndex { get; set; }
        private ZzukBot.Helpers.PPather.Path unitPath { get; set; }

        internal Tuple<bool, Location> ToUnit(WoWUnit parUnit)
        {
            if (parUnit == null)
                return null;

            if (ObjectManager.Instance.Player.Position.GetDistanceTo(parUnit.Position) < GrindSettings.Values.CtmStopDistance)
                return Tuple.Create(true, parUnit.Position);

            if (parUnit.Guid != unitLastGuid ||
                parUnit.Position.GetDistanceTo(unitLastPos) > 1.2f) 
            {
                if (!ObjectManager.Instance.Player.IsCtmIdle)
                {
                    var currentPlayer = ObjectManager.Instance.Player;
                    Location estimatedLocation = new Location(0, 0, 0);
                    //PPather is slow compared to mmaps, generate based on where we should be in 500ms
                    estimatedLocation.X = currentPlayer.Position.X + 5f*(float) Math.Cos(currentPlayer.Facing);
                    estimatedLocation.Y = currentPlayer.Position.Y + 5f*(float) Math.Sin(currentPlayer.Facing);

                    unitPath = Navigation.Instance.CalculatePath(estimatedLocation, parUnit.Position);
                }
                else
                {
                    unitPath = Navigation.Instance.CalculatePath(ObjectManager.Instance.Player.Position, parUnit.Position);
                }

                unitLastWaypointIndex = 0;
                unitLastPos = parUnit.Position;
                unitLastGuid = parUnit.Guid;
                lastUpdate = Environment.TickCount + 600;
            }
            //Path generation failed
            if (unitPath == null)
                return null;
            
            if (unitPath.Count() > 0)
            {
                if (Grinder.Access.Info.Waypoints.
                    NeedToLoadNextWaypoint(unitPath.Get(unitLastWaypointIndex)))
                {
                    var tmp = unitLastWaypointIndex + 1;
                    if (tmp != unitPath.Count())
                        unitLastWaypointIndex = tmp;
                }
            }
            var nextPoint = unitPath.Get(unitLastWaypointIndex);
            return Tuple.Create(true, nextPoint);
        }
    }
}
