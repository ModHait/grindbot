﻿using System;
using System.Collections.Generic;
using System.Linq;
using GrindBot.Engine.Grind;
using GrindBot.Settings;
using ZzukBot.Constants;
using ZzukBot.Helpers;
using ZzukBot.Game.Statics;
using ZzukBot.Mem;
using ZzukBot.Helpers.PPather;

namespace ZzukBot.Engines.Grind.Info.Path
{
    internal class _PathSafeGhostwalk
    {
        private readonly Random _random = new Random();
        private double angleRandomiser = 0.2;
        private bool ArrivedAtLastPoint;
        private int currentSafeIndex;
        private bool FoundSafePath;

        private List<Safespot> listSpots = new List<Safespot>();
        private ZzukBot.Helpers.PPather.Path ResurrectSafePath;

        internal Tuple<Location, bool> NextSafeWaypoint
        {
            get
            {
                var diff = 1.2f;
                if (currentSafeIndex == ResurrectSafePath.Count())
                    diff = 1.8f;

                if (!ArrivedAtLastPoint &&
                    Grinder.Access.Info.Waypoints.NeedToLoadNextWaypoint(ResurrectSafePath.Get(currentSafeIndex), diff, true))
                {
                    if (currentSafeIndex < ResurrectSafePath.Count() - 1)
                        currentSafeIndex++;
                    else
                    {
                        ArrivedAtLastPoint = true;
                    }
                }

                return Tuple.Create(ResurrectSafePath.Get(currentSafeIndex), ArrivedAtLastPoint);
            }
        }

        internal void Reset()
        {
            ArrivedAtLastPoint = false;
            ResurrectSafePath = null;
        }

        internal bool FindSafePath()
        {
            if (ResurrectSafePath != null)
            {
                return FoundSafePath;
            }

            var pos = ObjectManager.Instance.Player.CorpsePosition;
            var mobs = ObjectManager.Instance.Npcs
                .Where(i => i.Reaction == Enums.UnitReaction.Hostile &&
                            pos.GetDistanceTo(i.Position) < 40).ToList();

            var attempts = 0;
            angleRandomiser = 0.2;
            listSpots.Clear();
            while (true)
            {
                //XYZ[] superRandom = Navigation.GetRandomPath(pos, GameConstants.MaxResurrectDistance);
                //XYZ safeRezzSpot = superRandom[superRandom.Length - 1];
                //XYZ randomPoint = Navigation.GetRandomPoint(pos, GameConstants.MaxResurrectDistance);

                var angle = angleRandomiser * Math.PI * 2;
                var radius = Math.Sqrt(_random.NextDouble()) * (_random.Next(70, 100) / (float)100 * 40);
                var x = pos.X + radius * Math.Cos(angle);
                var y = pos.Y + radius * Math.Sin(angle);
                var randomPoint = new Location((float)x, (float)y, (float)pos.Z);
                angleRandomiser += 0.2;
                var hostileCount = mobs
                    .Where(i => i.Position.GetDistanceTo(randomPoint) < GrindSettings.Values.RezzDistanceToHostile)
                    .ToArray()
                    .Length;

                if (hostileCount == 0)
                {
                    var superRandom = Navigation.Instance.CalculatePath(pos, randomPoint);
                    if (superRandom.GetLast().GetDistanceTo(randomPoint) >= 2 ||
                        Math.Abs(superRandom.GetLast().Z - ObjectManager.Instance.Player.CorpsePosition.Z) > 15 ||
                        superRandom.GetLast().GetDistanceTo(ObjectManager.Instance.Player.CorpsePosition) >=
                        GrindSettings.Values.MaxResurrectDistance
                        )
                    {
                        attempts++;
                        continue;
                    }
                    ResurrectSafePath = superRandom;
                    FoundSafePath = true;
                    currentSafeIndex = 1;
                    return true;
                }
                listSpots.Add(new Safespot(hostileCount, randomPoint));
                attempts++;

                if (attempts == 500)
                {
                    //FoundSafePath = false;
                    //ResurrectSafePath = new XYZ[0];
                    break;
                }
            }

            listSpots = listSpots.OrderBy(i => i.HostileUnits).ToList();
            foreach (var x in listSpots)
            {
                var superRandom = Navigation.Instance.CalculatePath(pos, x.Position);
                if (superRandom.GetLast().GetDistanceTo(x.Position) >= 2 ||
                    Math.Abs(superRandom.GetLast().Z - ObjectManager.Instance.Player.CorpsePosition.Z) > 15 ||
                    superRandom.GetLast().GetDistanceTo(ObjectManager.Instance.Player.CorpsePosition) >=
                    GrindSettings.Values.MaxResurrectDistance
                    )
                {
                    continue;
                }
                ResurrectSafePath = superRandom;
                FoundSafePath = true;
                currentSafeIndex = 1;
                return true;
            }

            FoundSafePath = false;
            ResurrectSafePath = new Helpers.PPather.Path();
            return false;
        }

        private class Safespot
        {
            internal readonly int HostileUnits;
            internal readonly Location Position;

            internal Safespot(int parAggroUnits, Location parSpot)
            {
                Position = parSpot;
                HostileUnits = parAggroUnits;
            }
        }
    }
}