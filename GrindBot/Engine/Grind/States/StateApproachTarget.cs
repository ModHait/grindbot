﻿using System;
using Grindbot.Objects;
using GrindBot.Settings;
using ZzukBot.ExtensionFramework;
using ZzukBot.Objects;
using ZzukBot.Game.Statics;
using ZzukBot.ExtensionFramework.Classes;
using ZzukBot.Helpers.PPather;
using ZzukBot.Mem;

namespace GrindBot.Engine.Grind.States
{
    internal class StateApproachTarget : State
    {
        private readonly Random ran = new Random();
        private WoWUnit target;
        private Location lastMovement = null;

        internal override int Priority => 34;

        internal override bool NeedToRun => Grinder.Access.Info.Target.ShouldAttackTarget;

        internal override string Name => "Approaching Target";

        internal override void Run()
        {
            target = ObjectManager.Instance.Target;
            if (target == null) return;

            if (Grinder.Access.Info.Combat.IsBlacklisted(target)) return;
            //Grinder.Access.Info.Target.SearchDirect = true;

            //Grinder.Access.PrintToChat("Approach Target.");

            var player = ObjectManager.Instance.Player;
            var distanceToTarget = player.Position.GetDistanceTo(target.Position);

            if (distanceToTarget >= (Grinder.Access.Info.Target.CombatDistance-2) && ((player.Casting == 0
                                    && !Grinder.Access.Info.Combat.IsMoving) 
                                    || !Grinder.Access.Info.Target.InSightWithTarget))
            {

                var tu = Grinder.Access.Info.PathToUnit.ToUnit(target);

                if (tu == null)
                {
                    //Path generation has failed - try to ctm ontop of unit
                    ObjectManager.Instance.Player.CtmTo(target.Position);
                    return;
                }

                if (tu.Item1)
                {
                    player.CtmTo(tu.Item2);
                }
            }
            else
            {
                if (Grinder.Access.Info.Target.FixFacing)
                {
                    Grinder.Access.Info.Target.FixFacing = false;
                }
            }

            CustomClasses.Instance.Current.OnPull();
        }
    }
}