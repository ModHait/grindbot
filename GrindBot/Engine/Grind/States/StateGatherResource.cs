﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using Grindbot.Objects;
using GrindBot.Settings;
using ZzukBot.Objects;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers;

namespace GrindBot.Engine.Grind.States
{
    class StateGatherResource : State
    {
        internal override int Priority => 43;

        internal override bool NeedToRun => this.CanGather() || TargetNode != null;

        internal override string Name => "Gathering";


        internal override void Run()
        {
            if (TargetNode.Position.GetDistanceTo(ObjectManager.Instance.Player.Position) <= 4 /*interact dist*/)
            {
                //interact
            }
            else
            {
                //Pathing (look at repair)
            }
        }

        //Current Goal Node
        private WoWGameObject TargetNode = null;

        //PathToNode
        //Path

        //Blacklist node types

        //Blacklist specfic nodes
        private List<ulong> BlackListedNodes = new List<ulong>(); 

        private bool CanGather()
        {
            if (TargetNode != null)
            {
                //We have a node - maybe peform checks here?
                return true;
            }

            //Can't do this every frame, will be too slow
            if (!Wait.For("GatherCheck", 15000)) return false;
            Console.WriteLine("Checking for gather nodes.");

            if (GrindSettings.Values.Herb || GrindSettings.Values.Mine)
            {
                //If we see a node, isn't blacklisted & in range
                List<WoWGameObject> possibleNodes = ObjectManager.Instance.GameObjects;

                //Sort the nodes by distance (Hopefully not too slowly!)
                possibleNodes = possibleNodes.Where(i => !BlackListedNodes.Contains(i.Guid)
                && (Constants.Strings.HerbNames.Contains(i.Name) && GrindSettings.Values.Herb
                || Constants.Strings.MineNames.Contains(i.Name) && GrindSettings.Values.Mine)
                && i.DistanceTo(ObjectManager.Instance.Player) < GrindSettings.Values.MaxResourceDistance).
                OrderBy(i => i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position)).ToList();

                //If we have a node
                if (possibleNodes.Count != 0)
                {
                    //possibleNodes[0];

                    //LOS Check?
                    //Path Steps Check?
                    //Some Fucking Check
                    //Blacklist or set TargetNode
                }

            }
            return false;
        }
    }
}
