﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GrindBot.Helpers
{
    static class Statics
    {
        public static void LogToFile(string parFile, string parContent)
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "..\\" + parFile;
            File.AppendAllText(path, Environment.TickCount + @": " + parContent /*+ "\n" + @"###" + "\n" + "\n"*/);
        }
    }
}
