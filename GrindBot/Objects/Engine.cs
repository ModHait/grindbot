﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Grindbot.Objects;
using GrindBot.Engine;
using GrindBot.Engine.Grind;

namespace GrindBot.Objects
{
    internal class _Engine
    {
        String seperator = "|---------------------------------------------------------------|";
        internal _Engine(List<State> parStates)
        {
            States = parStates;
            // Remember: We implemented the IComparer, and IComparable
            // interfaces on the State class!
        }

        internal List<State> States { get; }
        internal bool Running { get; }

        internal virtual string Pulse()
        {
            int then = Environment.TickCount;
            bool checkstate = false;
            string Name = "";
            // This starts at the highest priority state,
            // and iterates its way to the lowest priority.
            foreach (var state in States)
            {
                try
                {
                    if (state.NeedToRun)
                    {
                        state.Run();
                        return state.Name;
                        // Break out of the iteration,
                        // as we found a state that has run.
                        // We don't want to run any more states
                        // this time around.
                    }
                }
                catch (Exception e)
                {
                    Grinder.Access.PrintToChat("Exception: "+ e.Message);
                    Grinder.Access.LogToFile("GrindException", e.Message + '\n' + e.StackTrace + '\n' + seperator + '\n');
                }


            }
            return "Null";
        }
    }
}
